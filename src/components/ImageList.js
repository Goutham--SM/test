import '../App.css';
import React,{Fragment, useState} from 'react';
import { createApi } from "unsplash-js";
import Texts from './Text';

export type Photo = {
  id: number;
  width: number;
  height: number;
  urls: { large: string; regular: string; raw: string; small: string };
  color: string | null;
  user: {
    username: string;
    name: string;
  };
};

export const api = createApi({
  // Don't forget to set your access token here!
  // See https://unsplash.com/developers
  accessKey: "lN5ClRwNAK_jKcrL88E64HCMBN7ErW0QMOghxHlMRGE"
});

export function ImageList({ photo }) {
  const { user, urls } = photo;
  const [isShown, setIsShown] = useState(false);
  return (
    <Fragment>
      <div className="hovers">
        <img className="img" src={urls.small}
        onMouseEnter={() => setIsShown(true)}
        onMouseLeave={() => setIsShown(false)} />
        {isShown&&(
        <div className="after">
          <Texts text= {user.name}/>
        </div>)}
      </div>
    </Fragment>
  );
}
