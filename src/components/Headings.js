import '../App.css';
import React from 'react';

function Headings(props) {
  return (
    <div className="App">
      <h1>{props.text}</h1>
    </div>
  );
}
export default Headings;
