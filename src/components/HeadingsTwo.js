import '../App.css';
import React from 'react';

function HeadingsTwo(props) {
  return (
    <div className="App">
      <h2>{props.text}</h2>
    </div>
  );
}
export default HeadingsTwo;
