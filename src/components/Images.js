import '../App.css';
import React, {useState,useEffect} from 'react';
import {ImageList, Photo, api} from './ImageList';
import { createApi } from "unsplash-js";
import HeadingsTwo from './HeadingsTwo';
const Images = () => {
  const [data, setPhotosResponse] = useState(null);

  useEffect(() => {
    api.search
      .getPhotos({ query: "web", orientation: "landscape", perPage:50})
      .then(result => {

        setTimeout(() => {
        setPhotosResponse(result);
      },1500);
    })
      .catch(() => {
        console.log("something went wrong!");
      });
  }, []);

  if (data === null) {
    return <HeadingsTwo text="Please wait until the image gets fetched from the server...."/>;
  } else if (data.errors) {
    return (
      <div>
        <div>{data.errors[0]}</div>
        <div>PS: Make sure to set your access token!</div>
      </div>
    );
  } else {
    return (
      <div className="feed">
        <ul className="grid_list">
          {data.response.results.map(photo => (
            <li key={photo.id} className="li">
              <ImageList photo={photo} />
            </li>
          ))}
        </ul>
      </div>
    );
  }
};

export default Images;
