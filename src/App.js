import Headings from './components/Headings';
import Texts from './components/Text';
import Images from './components/Images';
import React, {useState, Fragment} from 'react';
import './App.css';


function App() {
  const {view, setView} = useState(false);

  return (
    <div className="App">
      <Fragment>
        <Headings text='Goutham - Multimedia Gallery'/>
        <Texts text="The api I used to create this app is from unsplash which is a free and it allows the developer to  create this Gallery."/>
        <Images/>
        <Headings text='Goutham - Multimedia Gallery'/>
        <Headings text="Welcome."/>
      </Fragment>
    </div>
  );
}

export default App;
